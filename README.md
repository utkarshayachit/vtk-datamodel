![VTK - The Visualization Toolkit](vtkBanner.gif)

Introduction
============

This is VTK edition with modules needed for VTK data model. This edition also includes
the vtkGenericDataArray infrastructure.

License
=======

VTK is distributed under the OSI-approved BSD 3-clause License.
See [Copyright.txt][] for details.

[Copyright.txt]: Copyright.txt
